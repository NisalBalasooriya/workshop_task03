import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

public class PlayerController {
	
	private PlayerModel model;
    private PlayerView view;
    private Scanner scanner;

    public PlayerController(PlayerModel model, PlayerView view) {
        this.model = model;
        this.view = view;
        this.scanner = new Scanner(System.in);

        view.addStartButtonListener(new StartButtonListener());
    }
    
    public class StartButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
        	view.displayWelcomeMessage("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe\n"
        		    + "\nVersion 1.0 (c), Kevan Buckley, 2010");
               System.out.print("Enter your name: ");
               String playerName = scanner.nextLine();
               model.setPlayerName(playerName);

        }
      }
}
