import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PlayerView extends JFrame{
	
	private JLabel label;
	private JButton StartButton;
	
	public PlayerView() {
		
		setTitle("Abominodo");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(900, 400)); 
        setResizable(false); 
        setLocationRelativeTo(null);

        label = new JLabel();
        StartButton = new JButton("Start");

        setLayout(new FlowLayout());
        add(StartButton);
        add(label);

        pack();
        setVisible(true);
		
	}
	
	public void addStartButtonListener(ActionListener listener) {
        StartButton.addActionListener(listener);
    }
	
	public void displayWelcomeMessage(String welcome) {
    	label.setText(welcome);
    }

}
