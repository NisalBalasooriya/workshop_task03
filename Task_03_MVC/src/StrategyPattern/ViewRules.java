package StrategyPattern;

public class ViewRules implements Strategy {

	@Override
	public String autoPlay() {
		return("Displaying game rules...");
	}

}
