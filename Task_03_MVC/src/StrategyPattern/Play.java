package StrategyPattern;

public class Play implements Strategy{

	@Override
	public String autoPlay() {
		return("Starting the game...");	
	}

}
