package StrategyPattern;

import java.util.Scanner;

public class StrategyDemo {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println();
	      String h1 = "Main menu";
	      String u1 = h1.replaceAll(".", "=");
	      System.out.println(u1);
	      System.out.println(h1);
	      System.out.println(u1);
	      System.out.println("1) Play");
	      System.out.println("2) View high scores");
	      System.out.println("3) View rules");
	      System.out.println("0) Quit");
	      
	      int input = scanner.nextInt();
	      
	      scanner.close();
	      
	      if(input == 1) {
	    	  Context context = new Context(new Play());
	  		  System.out.println(context.executeStrategy());
	      }else if(input == 2) {
	    	  Context context = new Context(new ViewScore());
	  		  System.out.println(context.executeStrategy());  
	      }else if(input == 3) {
	    	  Context context = new Context(new ViewRules());
	  		  System.out.println(context.executeStrategy());    
	      }else if(input == 4) {
	    	  Context context = new Context(new Quit());
	  		  System.out.println(context.executeStrategy());    
	      }else {
	    	  System.out.println("Wrong input");
	      }
		
	}

}
