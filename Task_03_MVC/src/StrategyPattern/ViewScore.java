package StrategyPattern;

public class ViewScore implements Strategy {

	@Override
	public String autoPlay() {
		 return("Displaying high scores...");
		
	}

}
