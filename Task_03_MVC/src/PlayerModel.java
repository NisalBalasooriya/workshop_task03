import java.util.ArrayList;
import java.util.List;

public class PlayerModel {
	
	private List<PlayerObserver> players = new ArrayList<>();
    private String playerName;

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
        notifyPlayers();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void registerPlayer(PlayerObserver player) {
        players.add(player);
    }

    public void removePlayer(PlayerObserver player) {
        players.remove(player);
    }

    public void notifyPlayers() {
        for (PlayerObserver player : players) {
            player.update(playerName);
        }
    }

}
