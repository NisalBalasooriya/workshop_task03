package CommandPattern;

public class PlaceDominoCommand implements Command{
	private GameBoard gameBoard;
    private Domino domino;

    public PlaceDominoCommand(GameBoard gameBoard, Domino domino) {
        this.gameBoard = gameBoard;
        this.domino = domino;
    }

	@Override
	public void execute() {
		gameBoard.placeDomino(domino);
		
	}

	@Override
	public void undo() {
		gameBoard.removeDomino(domino);
		
	}

	@Override
	public int getScoreChange() {
		return domino.getScore();
	}


}
