package CommandPattern;

public class Main {
	public static void main(String[] args) {
        GameBoard gameBoard = new GameBoard();
        CommandInvoker invoker = new CommandInvoker();

        Domino domino1 = new Domino(3, 4);
        Domino domino2 = new Domino(2, 5);

        Command placeDomino1 = new PlaceDominoCommand(gameBoard, domino1);
        Command placeDomino2 = new PlaceDominoCommand(gameBoard, domino2);

        // Execute commands
        invoker.executeCommand(placeDomino1);
        invoker.executeCommand(placeDomino2);

        // Undo the last command
        invoker.undoLastCommand();

        // Print the placed dominoes
        System.out.println("Placed dominoes: " + gameBoard.getPlacedDominoes());
    }

}
