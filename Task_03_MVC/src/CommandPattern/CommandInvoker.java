package CommandPattern;

import java.util.ArrayList;
import java.util.List;

public class CommandInvoker {
	private List<Command> commandHistory;
    private int score;

    public CommandInvoker() {
        commandHistory = new ArrayList<>();
        score = 0;
    }

    public void executeCommand(Command command) {
        command.execute();
        commandHistory.add(command);
        score += command.getScoreChange();
        System.out.println("Current score: " + score);
    }

    public void undoLastCommand() {
        if (!commandHistory.isEmpty()) {
            Command lastCommand = commandHistory.remove(commandHistory.size() - 1);
            lastCommand.undo();
            score -= lastCommand.getScoreChange();
            System.out.println("Current score: " + score);
        } else {
            System.out.println("No commands to undo.");
        }
    }

}
