package CommandPattern;

public class Domino {
	
	 private int side1;
	    private int side2;

	    public Domino(int side1, int side2) {
	        this.side1 = side1;
	        this.side2 = side2;
	    }

	    @Override
	    public String toString() {
	        return "[" + side1 + "|" + side2 + "]";
	    }

	public int getScore() {
		return 10;
	}

}
