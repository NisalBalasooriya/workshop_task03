package CommandPattern;

import java.util.ArrayList;
import java.util.List;

public class GameBoard {
	private List<Domino> placedDominoes;
    private int score;

    public GameBoard() {
        placedDominoes = new ArrayList<>();
        score = 0;
    }

    public void placeDomino(Domino domino) {
        placedDominoes.add(domino);
        score += domino.getScore();
        System.out.println("Domino placed: " + domino);
        System.out.println("Score: " + score);
    }

    public void removeDomino(Domino domino) {
        placedDominoes.remove(domino);
        score -= domino.getScore();
        System.out.println("Domino removed: " + domino);
        System.out.println("Score: " + score);
    }

    public int getScore() {
        return score;
    }

    public List<Domino> getPlacedDominoes() {
        return placedDominoes;
    }

}
