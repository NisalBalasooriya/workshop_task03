
public class PlayerDemo {
	
	public static void main(String[] args) {
		PlayerModel model = new PlayerModel();
        PlayerView view = new PlayerView();
        PlayerController controller = new PlayerController(model, view);

        model.registerPlayer(new PlayerConsolePlayerObserver());
	}

}
