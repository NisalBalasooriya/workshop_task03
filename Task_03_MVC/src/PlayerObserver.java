
public interface PlayerObserver {
	
	void update(String playerName);

}
